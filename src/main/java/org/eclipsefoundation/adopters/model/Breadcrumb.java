/**
 * Copyright (c) 2023 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.adopters.model;

/**
 * Quick model to be used in displaying breadcrumb links in the Qute templates. Will be moved to commons sometime in the
 * future.
 * 
 * @author Martin Lowe
 */
public class Breadcrumb {
    public final String link;
    public final String label;

    public Breadcrumb(String link, String label) {
        this.link = link;
        this.label = label;
    }
}
