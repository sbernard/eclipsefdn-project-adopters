/*********************************************************************
* Copyright (c) 2020, 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*		Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.adopters.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.google.auto.value.AutoValue;

/**
 * Represents an adopter from the serialized adopter.json file.
 * 
 * @author Martin Lowe
 *
 */
@AutoValue
@JsonDeserialize(builder = AutoValue_Adopter.Builder.class)
public abstract class Adopter {
	public abstract String getName();

	@JsonProperty("homepage_url")
	public abstract String getHomepageUrl();

	public abstract String getLogo();

	@JsonProperty("logo_white")
	public abstract String getLogoWhite();

	public abstract List<String> getProjects();

	public static Builder builder() {
		return new AutoValue_Adopter.Builder();
	}

	@AutoValue.Builder
	@JsonPOJOBuilder(withPrefix = "set")
	public abstract static class Builder {

		public abstract Builder setName(String name);

		@JsonProperty("homepage_url")
		public abstract Builder setHomepageUrl(String url);

		public abstract Builder setLogo(String logo);

		@JsonProperty("logo_white")
		public abstract Builder setLogoWhite(String logoWhite);

		public abstract Builder setProjects(List<String> projects);

		public abstract Adopter build();

	}
}
