/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.adopters.precaches;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.adopters.api.WorkingGroupsAPI;
import org.eclipsefoundation.adopters.api.models.WorkingGroup;
import org.eclipsefoundation.core.model.ParameterizedCacheKey;
import org.eclipsefoundation.core.service.APIMiddleware;
import org.eclipsefoundation.core.service.LoadingCacheManager.LoadingCacheProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Named("working-groups")
@ApplicationScoped
public class WorkingGroupPrecacheProvider implements LoadingCacheProvider<WorkingGroup> {
    private static final Logger LOGGER = LoggerFactory.getLogger(WorkingGroupPrecacheProvider.class);

    @RestClient
    WorkingGroupsAPI api;

    @Inject
    APIMiddleware middleware;

    @Override
    public List<WorkingGroup> fetchData(ParameterizedCacheKey k) {
        LOGGER.debug("LOADING PROJECTS WITH KEY: {}", k);
        return middleware.getAll(params -> api.getAll(params, null), WorkingGroup.class);
    }

    @Override
    public Class<WorkingGroup> getType() {
        return WorkingGroup.class;
    }
}
